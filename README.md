# adepto

[![Gitter](https://badges.gitter.im/carddammom/adepto.svg)](https://gitter.im/carddammom/adepto?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)
[![Dependency Status](https://www.versioneye.com/user/projects/56fa5a9f35630e003888ad46/badge.svg?style=flat)](https://www.versioneye.com/user/projects/56fa5a9f35630e003888ad46)

Adepto is a android application to view information about your favourite sports team and they position in the different
sports they play (for example, soccer, voleyball, etc.).

Currently it aims to support only Portuguese teams.