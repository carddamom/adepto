package cc.cinnamon.android.adepto.beans.bulk

import cc.cinnamon.android.adepto.beans.JogadorEquipaType

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement
import javax.xml.bind.annotation.XmlType

/**
 * <p>Java class for anonymous complex type.
 * <p>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="jogador_equipa" type="{}jogadorEquipaType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = ["jogadorEquipa"])
@XmlRootElement(name = "jogadores_equipas")
public class JogadoresEquipas
{

	@XmlElement(name = "jogador_equipa")
	protected List<JogadorEquipaType> jogadorEquipa;

	/**
	 * Gets the value of the jogadorEquipa property.
	 * <p>
	 * <p>
	 * This accessor method returns a reference to the live list,
	 * not a snapshot. Therefore any modification you make to the
	 * returned list will be present inside the JAXB object.
	 * This is why there is not a <CODE>set</CODE> method for the jogadorEquipa property.
	 * <p>
	 * <p>
	 * For example, to add a new item, do as follows:
	 * <pre>
	 *    getJogadorEquipa().add(newItem);
	 * </pre>
	 * <p>
	 * <p>
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link JogadorEquipaType}
	 */
	public List<JogadorEquipaType> getJogadorEquipa()
	{
		if(jogadorEquipa == null)
		{
			jogadorEquipa = new ArrayList<JogadorEquipaType>();
		}
		return this.jogadorEquipa;
	}

}
