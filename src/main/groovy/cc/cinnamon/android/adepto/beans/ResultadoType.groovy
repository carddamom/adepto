package cc.cinnamon.android.adepto.beans

import jodd.vtor.constraint.NotNull

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlType

/**
 * <p>Java class for resultadoType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="resultadoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="jogo" type="{}jogoType"/&gt;
 *         &lt;element name="equipa" type="{}equipaType"/&gt;
 *         &lt;element name="resultado" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="final" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="descricao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "resultadoType", propOrder = [
		"id",
		"jogo",
		"equipa",
		"resultado",
		"_final",
		"descricao"
])
public class ResultadoType
{

	@XmlElement(required = true)
	@NotNull
	protected BigInteger id;

	@XmlElement(required = true)
	@NotNull
	protected JogoType jogo;

	@XmlElement(required = true)
	@NotNull
	protected EquipaType equipa;

	@XmlElement(required = true)
	@NotNull
	protected BigInteger resultado;

	@XmlElement(name = "final")
	@NotNull
	protected Boolean _final;

	protected String descricao;

	/**
	 * Gets the value of the id property.
	 *
	 * @return possible object is
	 * {@link BigInteger}
	 */
	public BigInteger getId()
	{
		return id;
	}

	/**
	 * Sets the value of the id property.
	 *
	 * @param value allowed object is
	 * {@link BigInteger}
	 */
	public void setId(BigInteger value)
	{
		this.id = value;
	}

	/**
	 * Gets the value of the jogo property.
	 *
	 * @return possible object is
	 * {@link JogoType}
	 */
	public JogoType getJogo()
	{
		return jogo;
	}

	/**
	 * Sets the value of the jogo property.
	 *
	 * @param value allowed object is
	 * {@link JogoType}
	 */
	public void setJogo(JogoType value)
	{
		this.jogo = value;
	}

	/**
	 * Gets the value of the equipa property.
	 *
	 * @return possible object is
	 * {@link EquipaType}
	 */
	public EquipaType getEquipa()
	{
		return equipa;
	}

	/**
	 * Sets the value of the equipa property.
	 *
	 * @param value allowed object is
	 * {@link EquipaType}
	 */
	public void setEquipa(EquipaType value)
	{
		this.equipa = value;
	}

	/**
	 * Gets the value of the resultado property.
	 *
	 * @return possible object is
	 * {@link BigInteger}
	 */
	public BigInteger getResultado()
	{
		return resultado;
	}

	/**
	 * Sets the value of the resultado property.
	 *
	 * @param value allowed object is
	 * {@link BigInteger}
	 */
	public void setResultado(BigInteger value)
	{
		this.resultado = value;
	}

	/**
	 * Gets the value of the final property.
	 */
	public Boolean isFinal()
	{
		return _final;
	}

	/**
	 * Sets the value of the final property.
	 */
	public void setFinal(Boolean value)
	{
		this._final = value;
	}

	/**
	 * Gets the value of the descricao property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getDescricao()
	{
		return descricao;
	}

	/**
	 * Sets the value of the descricao property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setDescricao(String value)
	{
		this.descricao = value;
	}

}
