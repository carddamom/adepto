package cc.cinnamon.android.adepto.beans

import cc.cinnamon.android.adepto.validation.Pattern
import jodd.vtor.constraint.NotNull

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlType

/**
 * <p>Java class for noticiasType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="noticiasType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="clube" type="{}clubeType"/&gt;
 *         &lt;element name="fonte" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="titulo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="resumo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="link"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyURI"&gt;
 *               &lt;pattern value="https?://.+"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="lida" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "noticiasType", propOrder = [
		"id",
		"clube",
		"fonte",
		"titulo",
		"resumo",
    "link",
    "lida"
])
public class NoticiasType {

	@XmlElement(required = true)
	@NotNull
	protected BigInteger id;

	@XmlElement(required = true)
	@NotNull
	protected ClubeType clube;

	protected String fonte;

	@XmlElement(required = true)
	@NotNull
	protected String titulo;

	@XmlElement(required = true)
	@NotNull
	protected String resumo;

	@XmlElement(required = true)
	@NotNull
	@Pattern(regexp = "https?://.+")
	protected String link;

    protected Boolean lida;

	/**
	 * Gets the value of the id property.
	 *
	 * @return possible object is
	 * {@link BigInteger}
	 */
	public BigInteger getId()
	{
		return id;
	}

	/**
	 * Sets the value of the id property.
	 *
	 * @param value allowed object is
	 * {@link BigInteger}
	 */
	public void setId(BigInteger value)
	{
		this.id = value;
	}

	/**
	 * Gets the value of the clube property.
	 *
	 * @return possible object is
	 * {@link ClubeType}
	 */
	public ClubeType getClube()
	{
		return clube;
	}

	/**
	 * Sets the value of the clube property.
	 *
	 * @param value allowed object is
	 * {@link ClubeType}
	 */
	public void setClube(ClubeType value)
	{
		this.clube = value;
	}

	/**
	 * Gets the value of the fonte property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getFonte()
	{
		return fonte;
	}

	/**
	 * Sets the value of the fonte property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setFonte(String value)
	{
		this.fonte = value;
	}

	/**
	 * Gets the value of the titulo property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getTitulo()
	{
		return titulo;
	}

	/**
	 * Sets the value of the titulo property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setTitulo(String value)
	{
		this.titulo = value;
	}

	/**
	 * Gets the value of the resumo property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getResumo()
	{
		return resumo;
	}

	/**
	 * Sets the value of the resumo property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setResumo(String value)
	{
		this.resumo = value;
	}

	/**
	 * Gets the value of the link property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getLink()
	{
		return link;
	}

	/**
	 * Sets the value of the link property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setLink(String value)
	{
		this.link = value;
	}

    /**
     * Gets the value of the lida property.
     *
     * @return possible object is {@link Boolean }
     *
     */
    public Boolean getLida() {
        return lida;
    }

    /**
     * Sets the value of the lida property.
     *
     * @param value allowed object is {@link Boolean }
     *
     */
    public void setLida(Boolean value) {
        this.lida = value;
    }

}
