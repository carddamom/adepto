package cc.cinnamon.android.adepto.beans

import jodd.vtor.constraint.NotNull

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlType

/**
 * <p>Java class for equipamentoType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="equipamentoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="equipamento" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/&gt;
 *         &lt;element name="descricao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "equipamentoType", propOrder = [
		"id",
		"equipamento",
		"descricao"
])
public class EquipamentoType
{

	@XmlElement(required = true)
	@NotNull
	protected BigInteger id;

	@XmlElement(required = true)
	@NotNull
	protected byte[] equipamento;

	protected String descricao;

	/**
	 * Gets the value of the id property.
	 *
	 * @return possible object is
	 * {@link BigInteger}
	 */
	public BigInteger getId()
	{
		return id;
	}

	/**
	 * Sets the value of the id property.
	 *
	 * @param value allowed object is
	 * {@link BigInteger}
	 */
	public void setId(BigInteger value)
	{
		this.id = value;
	}

	/**
	 * Gets the value of the equipamento property.
	 *
	 * @return possible object is
	 * byte[]
	 */
	public byte[] getEquipamento()
	{
		return equipamento;
	}

	/**
	 * Sets the value of the equipamento property.
	 *
	 * @param value allowed object is
	 *              byte[]
	 */
	public void setEquipamento(byte[] value)
	{
		this.equipamento = value;
	}

	/**
	 * Gets the value of the descricao property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getDescricao()
	{
		return descricao;
	}

	/**
	 * Sets the value of the descricao property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setDescricao(String value)
	{
		this.descricao = value;
	}

}
