package cc.cinnamon.android.adepto.beans

import jodd.vtor.constraint.NotNull

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlType

/**
 * <p>Java class for resultadoJogadorType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="resultadoJogadorType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="resultado" type="{}resultadoType"/&gt;
 *         &lt;element name="jogador" type="{}jogadorType"/&gt;
 *         &lt;element name="minutos" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="autogolo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "resultadoJogadorType", propOrder = [
		"id",
		"resultado",
		"jogador",
		"minutos",
		"autogolo"
])
public class ResultadoJogadorType
{

	@XmlElement(required = true)
	@NotNull
	protected BigInteger id;

	@XmlElement(required = true)
	@NotNull
	protected ResultadoType resultado;

	@XmlElement(required = true)
	@NotNull
	protected JogadorType jogador;

	@XmlElement(required = true)
	@NotNull
	protected BigInteger minutos;

	protected Boolean autogolo;

	/**
	 * Gets the value of the id property.
	 *
	 * @return possible object is
	 * {@link BigInteger}
	 */
	public BigInteger getId()
	{
		return id;
	}

	/**
	 * Sets the value of the id property.
	 *
	 * @param value allowed object is
	 * {@link BigInteger}
	 */
	public void setId(BigInteger value)
	{
		this.id = value;
	}

	/**
	 * Gets the value of the resultado property.
	 *
	 * @return possible object is
	 * {@link ResultadoType}
	 */
	public ResultadoType getResultado()
	{
		return resultado;
	}

	/**
	 * Sets the value of the resultado property.
	 *
	 * @param value allowed object is
	 * {@link ResultadoType}
	 */
	public void setResultado(ResultadoType value)
	{
		this.resultado = value;
	}

	/**
	 * Gets the value of the jogador property.
	 *
	 * @return possible object is
	 * {@link JogadorType}
	 */
	public JogadorType getJogador()
	{
		return jogador;
	}

	/**
	 * Sets the value of the jogador property.
	 *
	 * @param value allowed object is
	 * {@link JogadorType}
	 */
	public void setJogador(JogadorType value)
	{
		this.jogador = value;
	}

	/**
	 * Gets the value of the minutos property.
	 *
	 * @return possible object is
	 * {@link BigInteger}
	 */
	public BigInteger getMinutos()
	{
		return minutos;
	}

	/**
	 * Sets the value of the minutos property.
	 *
	 * @param value allowed object is
	 * {@link BigInteger}
	 */
	public void setMinutos(BigInteger value)
	{
		this.minutos = value;
	}

	/**
	 * Gets the value of the autogolo property.
     *
     * @return possible object is {@link Boolean }
     *
	 */
	public Boolean isAutogolo()
	{
		return autogolo;
	}

	/**
	 * Sets the value of the autogolo property.
     *
     * @param value allowed object is {@link Boolean }
     *
	 */
	public void setAutogolo(Boolean value)
	{
		this.autogolo = value;
	}

}
