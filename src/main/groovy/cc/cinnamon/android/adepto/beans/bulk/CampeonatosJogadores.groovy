package cc.cinnamon.android.adepto.beans.bulk

import cc.cinnamon.android.adepto.beans.CampeonatoJogadorType

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement
import javax.xml.bind.annotation.XmlType

/**
 * <p>Java class for anonymous complex type.
 * <p>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="campeonato_jogador" type="{}campeonatoJogadorType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = ["campeonatoJogador"])
@XmlRootElement(name = "campeonatos_jogadores")
public class CampeonatosJogadores
{

	@XmlElement(name = "campeonato_jogador")
	protected List<CampeonatoJogadorType> campeonatoJogador;

	/**
	 * Gets the value of the campeonatoJogador property.
	 * <p>
	 * <p>
	 * This accessor method returns a reference to the live list,
	 * not a snapshot. Therefore any modification you make to the
	 * returned list will be present inside the JAXB object.
	 * This is why there is not a <CODE>set</CODE> method for the campeonatoJogador property.
	 * <p>
	 * <p>
	 * For example, to add a new item, do as follows:
	 * <pre>
	 *    getCampeonatoJogador().add(newItem);
	 * </pre>
	 * <p>
	 * <p>
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link CampeonatoJogadorType}
	 */
	public List<CampeonatoJogadorType> getCampeonatoJogador()
	{
		if(campeonatoJogador == null)
		{
			campeonatoJogador = new ArrayList<CampeonatoJogadorType>();
		}
		return this.campeonatoJogador;
	}

}
