package cc.cinnamon.android.adepto.beans

import cc.cinnamon.android.adepto.validation.Pattern
import jodd.vtor.constraint.NotNull

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlType

/**
 * <p>Java class for recintoType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="recintoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="nome" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="morada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="freguesia" type="{}freguesiaType" minOccurs="0"/&gt;
 *         &lt;element name="codigo_postal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="localidade" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="website" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyURI"&gt;
 *               &lt;pattern value="https?://.+"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="foto" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "recintoType", propOrder = [
		"id",
		"nome",
		"morada",
		"freguesia",
		"codigoPostal",
		"localidade",
		"website",
		"foto"
])
public class RecintoType
{

	@XmlElement(required = true)
	@NotNull
	protected BigInteger id;

	@XmlElement(required = true)
	@NotNull
	protected String nome;

	protected String morada;

	protected FreguesiaType freguesia;

    @XmlElement(name = "codigo_postal")
	protected String codigoPostal;

	protected String localidade;

	@Pattern(regexp = "https?://.+")
	protected String website;

	protected byte[] foto;

	/**
	 * Gets the value of the id property.
	 *
	 * @return possible object is
	 * {@link BigInteger}
	 */
	public BigInteger getId()
	{
		return id;
	}

	/**
	 * Sets the value of the id property.
	 *
	 * @param value allowed object is
	 * {@link BigInteger}
	 */
	public void setId(BigInteger value)
	{
		this.id = value;
	}

	/**
	 * Gets the value of the nome property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getNome()
	{
		return nome;
	}

	/**
	 * Sets the value of the nome property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setNome(String value)
	{
		this.nome = value;
	}

	/**
	 * Gets the value of the morada property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getMorada()
	{
		return morada;
	}

	/**
	 * Sets the value of the morada property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setMorada(String value)
	{
		this.morada = value;
	}

	/**
	 * Gets the value of the freguesia property.
	 *
	 * @return possible object is
	 * {@link FreguesiaType}
	 */
	public FreguesiaType getFreguesia()
	{
		return freguesia;
	}

	/**
	 * Sets the value of the freguesia property.
	 *
	 * @param value allowed object is
	 * {@link FreguesiaType}
	 */
	public void setFreguesia(FreguesiaType value)
	{
		this.freguesia = value;
	}

	/**
	 * Gets the value of the codigoPostal property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getCodigoPostal()
	{
		return codigoPostal;
	}

	/**
	 * Sets the value of the codigoPostal property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setCodigoPostal(String value)
	{
		this.codigoPostal = value;
	}

	/**
	 * Gets the value of the localidade property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getLocalidade()
	{
		return localidade;
	}

	/**
	 * Sets the value of the localidade property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setLocalidade(String value)
	{
		this.localidade = value;
	}

	/**
	 * Gets the value of the website property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getWebsite()
	{
		return website;
	}

	/**
	 * Sets the value of the website property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setWebsite(String value)
	{
		this.website = value;
	}

	/**
	 * Gets the value of the foto property.
	 *
	 * @return possible object is
	 * byte[]
	 */
	public byte[] getFoto()
	{
		return foto;
	}

	/**
	 * Sets the value of the foto property.
	 *
	 * @param value allowed object is
	 *              byte[]
	 */
	public void setFoto(byte[] value)
	{
		this.foto = value;
	}

}
