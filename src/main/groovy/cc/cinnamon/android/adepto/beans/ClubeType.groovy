package cc.cinnamon.android.adepto.beans

import cc.cinnamon.android.adepto.validation.Pattern
import jodd.vtor.constraint.NotNull
import jodd.vtor.constraint.Size

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlSchemaType
import javax.xml.bind.annotation.XmlType
import javax.xml.datatype.XMLGregorianCalendar

/**
 * <p>Java class for clubeType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="clubeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="nome" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="abreviatura"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="3"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="morada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="localidade" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="contactos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="email" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;pattern value="[^@]+@[^\.]+\..+"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="data_fundacao" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="telefone" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="9"/&gt;
 *               &lt;pattern value="[0-9]+"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="fax" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="website" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyURI"&gt;
 *               &lt;pattern value="https?://.+"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="freguesia" type="{}freguesiaType" minOccurs="0"/&gt;
 *         &lt;element name="cor" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="logo" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "clubeType", propOrder = [
		"id",
		"nome",
		"abreviatura",
		"morada",
		"localidade",
		"contactos",
		"email",
		"dataFundacao",
		"telefone",
		"fax",
		"website",
		"freguesia",
		"cor",
		"logo"
])
public class ClubeType
{

	@XmlElement(required = true)
	@NotNull
	protected BigInteger id;

	@XmlElement(required = true)
	@NotNull
	protected String nome;

	@XmlElement(required = true)
	@NotNull
	@Size(max = 3)
	protected String abreviatura;

	protected String morada;

	protected String localidade;

	protected String contactos;

	@Pattern(regexp = "[^@]+@[^\\.]+\\..+")
	protected String email;

	@XmlElement(name = "data_fundacao", required = true)
	@XmlSchemaType(name = "date")
	@NotNull
	protected XMLGregorianCalendar dataFundacao;

	@Size(max = 9)
	@Pattern(regexp = "[0-9]+")
	protected String telefone;

	protected String fax;

	@Pattern(regexp = "https?://.+")
	protected String website;

	protected FreguesiaType freguesia;

	@XmlElement(required = true)
	@NotNull
	protected String cor;

	protected byte[] logo;

	/**
	 * Gets the value of the id property.
	 *
	 * @return possible object is
	 * {@link BigInteger}
	 */
	public BigInteger getId()
	{
		return id;
	}

	/**
	 * Sets the value of the id property.
	 *
	 * @param value allowed object is
	 * {@link BigInteger}
	 */
	public void setId(BigInteger value)
	{
		this.id = value;
	}

	/**
	 * Gets the value of the nome property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getNome()
	{
		return nome;
	}

	/**
	 * Sets the value of the nome property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setNome(String value)
	{
		this.nome = value;
	}

	/**
	 * Gets the value of the abreviatura property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getAbreviatura()
	{
		return abreviatura;
	}

	/**
	 * Sets the value of the abreviatura property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setAbreviatura(String value)
	{
		this.abreviatura = value;
	}

	/**
	 * Gets the value of the morada property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getMorada()
	{
		return morada;
	}

	/**
	 * Sets the value of the morada property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setMorada(String value)
	{
		this.morada = value;
	}

	/**
	 * Gets the value of the localidade property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getLocalidade()
	{
		return localidade;
	}

	/**
	 * Sets the value of the localidade property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setLocalidade(String value)
	{
		this.localidade = value;
	}

	/**
	 * Gets the value of the contactos property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getContactos()
	{
		return contactos;
	}

	/**
	 * Sets the value of the contactos property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setContactos(String value)
	{
		this.contactos = value;
	}

	/**
	 * Gets the value of the email property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * Sets the value of the email property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setEmail(String value)
	{
		this.email = value;
	}

	/**
	 * Gets the value of the dataFundacao property.
	 *
	 * @return possible object is
	 * {@link XMLGregorianCalendar}
	 */
	public XMLGregorianCalendar getDataFundacao()
	{
		return dataFundacao;
	}

	/**
	 * Sets the value of the dataFundacao property.
	 *
	 * @param value allowed object is
	 * {@link XMLGregorianCalendar}
	 */
	public void setDataFundacao(XMLGregorianCalendar value)
	{
		this.dataFundacao = value;
	}

	/**
	 * Gets the value of the telefone property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getTelefone()
	{
		return telefone;
	}

	/**
	 * Sets the value of the telefone property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setTelefone(String value)
	{
		this.telefone = value;
	}

	/**
	 * Gets the value of the fax property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getFax()
	{
		return fax;
	}

	/**
	 * Sets the value of the fax property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setFax(String value)
	{
		this.fax = value;
	}

	/**
	 * Gets the value of the website property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getWebsite()
	{
		return website;
	}

	/**
	 * Sets the value of the website property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setWebsite(String value)
	{
		this.website = value;
	}

	/**
	 * Gets the value of the freguesia property.
	 *
	 * @return possible object is
	 * {@link FreguesiaType}
	 */
	public FreguesiaType getFreguesia()
	{
		return freguesia;
	}

	/**
	 * Sets the value of the freguesia property.
	 *
	 * @param value allowed object is
	 * {@link FreguesiaType}
	 */
	public void setFreguesia(FreguesiaType value)
	{
		this.freguesia = value;
	}

	/**
	 * Gets the value of the cor property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getCor()
	{
		return cor;
	}

	/**
	 * Sets the value of the cor property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setCor(String value)
	{
		this.cor = value;
	}

	/**
	 * Gets the value of the logo property.
	 *
	 * @return possible object is
	 * byte[]
	 */
	public byte[] getLogo()
	{
		return logo;
	}

	/**
	 * Sets the value of the logo property.
	 *
	 * @param value allowed object is
	 *              byte[]
	 */
	public void setLogo(byte[] value)
	{
		this.logo = value;
	}

}
