package cc.cinnamon.android.adepto.beans

import jodd.vtor.constraint.NotNull

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlType

/**
 * <p>Java class for campeonatoType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="campeonatoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="nome" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="logotipo" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="patrocinador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="escalao" type="{}escalaoType"/&gt;
 *         &lt;element name="descricao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "campeonatoType", propOrder = [
		"id",
		"nome",
		"logotipo",
		"patrocinador",
		"escalao",
		"descricao"
])
public class CampeonatoType
{

	@XmlElement(required = true)
	@NotNull
	protected BigInteger id;

	@XmlElement(required = true)
	@NotNull
	protected String nome;

	protected byte[] logotipo;

	protected String patrocinador;

	@XmlElement(required = true)
	@NotNull
	protected EscalaoType escalao;

	protected String descricao;

	/**
	 * Gets the value of the id property.
	 *
	 * @return possible object is
	 * {@link BigInteger}
	 */
	public BigInteger getId()
	{
		return id;
	}

	/**
	 * Sets the value of the id property.
	 *
	 * @param value allowed object is
	 * {@link BigInteger}
	 */
	public void setId(BigInteger value)
	{
		this.id = value;
	}

	/**
	 * Gets the value of the nome property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getNome()
	{
		return nome;
	}

	/**
	 * Sets the value of the nome property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setNome(String value)
	{
		this.nome = value;
	}

	/**
	 * Gets the value of the logotipo property.
	 *
	 * @return possible object is
	 * byte[]
	 */
	public byte[] getLogotipo()
	{
		return logotipo;
	}

	/**
	 * Sets the value of the logotipo property.
	 *
	 * @param value allowed object is
	 *              byte[]
	 */
	public void setLogotipo(byte[] value)
	{
		this.logotipo = value;
	}

	/**
	 * Gets the value of the patrocinador property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getPatrocinador()
	{
		return patrocinador;
	}

	/**
	 * Sets the value of the patrocinador property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setPatrocinador(String value)
	{
		this.patrocinador = value;
	}

	/**
	 * Gets the value of the escalao property.
	 *
	 * @return possible object is
	 * {@link EscalaoType}
	 */
	public EscalaoType getEscalao()
	{
		return escalao;
	}

	/**
	 * Sets the value of the escalao property.
	 *
	 * @param value allowed object is
	 * {@link EscalaoType}
	 */
	public void setEscalao(EscalaoType value)
	{
		this.escalao = value;
	}

	/**
	 * Gets the value of the descricao property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getDescricao()
	{
		return descricao;
	}

	/**
	 * Sets the value of the descricao property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setDescricao(String value)
	{
		this.descricao = value;
	}

}
