package cc.cinnamon.android.adepto.beans

import jodd.vtor.constraint.NotNull

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlType

/**
 * <p>Java class for faseType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="faseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="epoca" type="{}epocaType"/&gt;
 *         &lt;element name="nome" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="descricao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "faseType", propOrder = [
		"id",
		"epoca",
		"nome",
		"descricao"
])
public class FaseType
{

	@XmlElement(required = true)
	@NotNull
	protected BigInteger id;

	@XmlElement(required = true)
	@NotNull
	protected EpocaType epoca;

	@XmlElement(required = true)
	@NotNull
	protected String nome;

	protected String descricao;

	/**
	 * Gets the value of the id property.
	 *
	 * @return possible object is
	 * {@link BigInteger}
	 */
	public BigInteger getId()
	{
		return id;
	}

	/**
	 * Sets the value of the id property.
	 *
	 * @param value allowed object is
	 * {@link BigInteger}
	 */
	public void setId(BigInteger value)
	{
		this.id = value;
	}

	/**
	 * Gets the value of the epoca property.
	 *
	 * @return possible object is
	 * {@link EpocaType}
	 */
	public EpocaType getEpoca()
	{
		return epoca;
	}

	/**
	 * Sets the value of the epoca property.
	 *
	 * @param value allowed object is
	 * {@link EpocaType}
	 */
	public void setEpoca(EpocaType value)
	{
		this.epoca = value;
	}

	/**
	 * Gets the value of the nome property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getNome()
	{
		return nome;
	}

	/**
	 * Sets the value of the nome property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setNome(String value)
	{
		this.nome = value;
	}

	/**
	 * Gets the value of the descricao property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getDescricao()
	{
		return descricao;
	}

	/**
	 * Sets the value of the descricao property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setDescricao(String value)
	{
		this.descricao = value;
	}

}
