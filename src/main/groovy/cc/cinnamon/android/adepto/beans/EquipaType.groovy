package cc.cinnamon.android.adepto.beans

import cc.cinnamon.android.adepto.validation.Pattern
import jodd.vtor.constraint.NotNull

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlType

/**
 * <p>Java class for equipaType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="equipaType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="clube" type="{}clubeType"/&gt;
 *         &lt;element name="nome" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="escalao" type="{}escalaoType"/&gt;
 *         &lt;element name="genero"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;enumeration value="F"/&gt;
 *               &lt;enumeration value="M"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="equipamento" type="{}equipamentoType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "equipaType", propOrder = [
		"id",
		"clube",
		"nome",
		"escalao",
		"genero",
		"equipamento"
])
public class EquipaType
{

	@XmlElement(required = true)
	@NotNull
	protected BigInteger id;

	@XmlElement(required = true)
	@NotNull
	protected ClubeType clube;

	@XmlElement(required = true)
	@NotNull
	protected String nome;

	@XmlElement(required = true)
	@NotNull
	protected EscalaoType escalao;

	@XmlElement(required = true)
	@NotNull
	@Pattern(regexp = ["F", "M"])
	protected String genero;

	protected EquipamentoType equipamento;

	/**
	 * Gets the value of the id property.
	 *
	 * @return possible object is
	 * {@link BigInteger}
	 */
	public BigInteger getId()
	{
		return id;
	}

	/**
	 * Sets the value of the id property.
	 *
	 * @param value allowed object is
	 * {@link BigInteger}
	 */
	public void setId(BigInteger value)
	{
		this.id = value;
	}

	/**
	 * Gets the value of the clube property.
	 *
	 * @return possible object is
	 * {@link ClubeType}
	 */
	public ClubeType getClube()
	{
		return clube;
	}

	/**
	 * Sets the value of the clube property.
	 *
	 * @param value allowed object is
	 * {@link ClubeType}
	 */
	public void setClube(ClubeType value)
	{
		this.clube = value;
	}

	/**
	 * Gets the value of the nome property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getNome()
	{
		return nome;
	}

	/**
	 * Sets the value of the nome property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setNome(String value)
	{
		this.nome = value;
	}

	/**
	 * Gets the value of the escalao property.
	 *
	 * @return possible object is
	 * {@link EscalaoType}
	 */
	public EscalaoType getEscalao()
	{
		return escalao;
	}

	/**
	 * Sets the value of the escalao property.
	 *
	 * @param value allowed object is
	 * {@link EscalaoType}
	 */
	public void setEscalao(EscalaoType value)
	{
		this.escalao = value;
	}

	/**
	 * Gets the value of the genero property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getGenero()
	{
		return genero;
	}

	/**
	 * Sets the value of the genero property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setGenero(String value)
	{
		this.genero = value;
	}

	/**
	 * Gets the value of the equipamento property.
	 *
	 * @return possible object is
	 * {@link EquipamentoType}
	 */
	public EquipamentoType getEquipamento()
	{
		return equipamento;
	}

	/**
	 * Sets the value of the equipamento property.
	 *
	 * @param value allowed object is
	 * {@link EquipamentoType}
	 */
	public void setEquipamento(EquipamentoType value)
	{
		this.equipamento = value;
	}

}
