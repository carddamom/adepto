package cc.cinnamon.android.adepto.beans

import jodd.vtor.constraint.NotNull

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlType

/**
 * <p>Java class for campeonatoJogadorType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="campeonatoJogadorType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="jogador" type="{}jogadorType"/&gt;
 *         &lt;element name="campeonato" type="{}campeonatoType"/&gt;
 *         &lt;element name="pontuacao" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "campeonatoJogadorType", propOrder = [
		"id",
		"jogador",
		"campeonato",
		"pontuacao"
])
public class CampeonatoJogadorType
{

	@XmlElement(required = true)
	@NotNull
	protected BigInteger id;

	@XmlElement(required = true)
	@NotNull
	protected JogadorType jogador;

	@XmlElement(required = true)
	@NotNull
	protected CampeonatoType campeonato;

	protected BigInteger pontuacao;

	/**
	 * Gets the value of the id property.
	 *
	 * @return possible object is
	 * {@link BigInteger}
	 */
	public BigInteger getId()
	{
		return id;
	}

	/**
	 * Sets the value of the id property.
	 *
	 * @param value allowed object is
	 * {@link BigInteger}
	 */
	public void setId(BigInteger value)
	{
		this.id = value;
	}

	/**
	 * Gets the value of the jogador property.
	 *
	 * @return possible object is
	 * {@link JogadorType}
	 */
	public JogadorType getJogador()
	{
		return jogador;
	}

	/**
	 * Sets the value of the jogador property.
	 *
	 * @param value allowed object is
	 * {@link JogadorType}
	 */
	public void setJogador(JogadorType value)
	{
		this.jogador = value;
	}

	/**
	 * Gets the value of the campeonato property.
	 *
	 * @return possible object is
	 * {@link CampeonatoType}
	 */
	public CampeonatoType getCampeonato()
	{
		return campeonato;
	}

	/**
	 * Sets the value of the campeonato property.
	 *
	 * @param value allowed object is
	 * {@link CampeonatoType}
	 */
	public void setCampeonato(CampeonatoType value)
	{
		this.campeonato = value;
	}

	/**
	 * Gets the value of the pontuacao property.
	 *
	 * @return possible object is
	 * {@link BigInteger}
	 */
	public BigInteger getPontuacao()
	{
		return pontuacao;
	}

	/**
	 * Sets the value of the pontuacao property.
	 *
	 * @param value allowed object is
	 * {@link BigInteger}
	 */
	public void setPontuacao(BigInteger value)
	{
		this.pontuacao = value;
	}

}
