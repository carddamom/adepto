package cc.cinnamon.android.adepto.beans

import jodd.vtor.constraint.NotNull

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlSchemaType
import javax.xml.bind.annotation.XmlType
import javax.xml.datatype.XMLGregorianCalendar

/**
 * <p>Java class for jogadorEquipaType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="jogadorEquipaType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="jogador" type="{}jogadorType"/&gt;
 *         &lt;element name="equipa" type="{}equipaType"/&gt;
 *         &lt;element name="posicao" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="capitao" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="data" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "jogadorEquipaType", propOrder = [
		"id",
		"jogador",
    "equipa",
		"posicao",
		"numero",
		"capitao",
		"data"
])
public class JogadorEquipaType
{

	@XmlElement(required = true)
	@NotNull
	protected BigInteger id;

	@XmlElement(required = true)
	@NotNull
	protected JogadorType jogador;

    @XmlElement(required = true)
    @NotNull
    protected EquipaType equipa;

	@XmlElement(required = true)
	@NotNull
	protected String posicao;

	@XmlElement(required = true)
	@NotNull
	protected BigInteger numero;

	protected Boolean capitao;

	@XmlElement(required = true)
	@XmlSchemaType(name = "date")
	@NotNull
	protected XMLGregorianCalendar data;

	/**
	 * Gets the value of the id property.
	 *
	 * @return possible object is
	 * {@link BigInteger}
	 */
	public BigInteger getId()
	{
		return id;
	}

	/**
	 * Sets the value of the id property.
	 *
	 * @param value allowed object is
	 * {@link BigInteger}
	 */
	public void setId(BigInteger value)
	{
		this.id = value;
	}

	/**
	 * Gets the value of the jogador property.
	 *
	 * @return possible object is
	 * {@link JogadorType}
	 */
	public JogadorType getJogador()
	{
		return jogador;
	}

	/**
	 * Sets the value of the jogador property.
	 *
	 * @param value allowed object is
	 * {@link JogadorType}
	 */
	public void setJogador(JogadorType value)
	{
		this.jogador = value;
	}

    /**
     * Gets the value of the equipa property.
     *
     * @return possible object is {@link EquipaType }
     *
     */
    public EquipaType getEquipa() {
        return equipa;
    }

    /**
     * Sets the value of the equipa property.
     *
     * @param value allowed object is {@link EquipaType }
     *
     */
    public void setEquipa(EquipaType value) {
        this.equipa = value;
    }

	/**
	 * Gets the value of the posicao property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getPosicao()
	{
		return posicao;
	}

	/**
	 * Sets the value of the posicao property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setPosicao(String value)
	{
		this.posicao = value;
	}

	/**
	 * Gets the value of the numero property.
	 *
	 * @return possible object is
	 * {@link BigInteger}
	 */
	public BigInteger getNumero()
	{
		return numero;
	}

	/**
	 * Sets the value of the numero property.
	 *
	 * @param value allowed object is
	 * {@link BigInteger}
	 */
	public void setNumero(BigInteger value)
	{
		this.numero = value;
	}

	/**
	 * Gets the value of the capitao property.
     *
     * @return
     *     possible object is {@link Boolean }
     *
	 */
	public Boolean isCapitao()
	{
		return capitao;
	}

	/**
	 * Sets the value of the capitao property.
     *
     * @param value allowed object is {@link Boolean }
     *
	 */
	public void setCapitao(Boolean value)
	{
		this.capitao = value;
	}

	/**
	 * Gets the value of the data property.
	 *
	 * @return possible object is
	 * {@link XMLGregorianCalendar}
	 */
	public XMLGregorianCalendar getData()
	{
		return data;
	}

	/**
	 * Sets the value of the data property.
	 *
	 * @param value allowed object is
	 * {@link XMLGregorianCalendar}
	 */
	public void setData(XMLGregorianCalendar value)
	{
		this.data = value;
	}

}
