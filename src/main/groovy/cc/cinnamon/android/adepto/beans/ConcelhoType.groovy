package cc.cinnamon.android.adepto.beans

import jodd.vtor.constraint.NotNull

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlType

/**
 * <p>Java class for concelhoType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="concelhoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="distrito" type="{}distritoType"/&gt;
 *         &lt;element name="nome" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "concelhoType", propOrder = [
		"id",
		"distrito",
		"nome"
])
public class ConcelhoType
{

	@XmlElement(required = true)
	@NotNull
	protected BigInteger id;

	@XmlElement(required = true)
	@NotNull
	protected DistritoType distrito;

	@XmlElement(required = true)
	@NotNull
	protected String nome;

	/**
	 * Gets the value of the id property.
	 *
	 * @return possible object is
	 * {@link BigInteger}
	 */
	public BigInteger getId()
	{
		return id;
	}

	/**
	 * Sets the value of the id property.
	 *
	 * @param value allowed object is
	 * {@link BigInteger}
	 */
	public void setId(BigInteger value)
	{
		this.id = value;
	}

	/**
	 * Gets the value of the distrito property.
	 *
	 * @return possible object is
	 * {@link DistritoType}
	 */
	public DistritoType getDistrito()
	{
		return distrito;
	}

	/**
	 * Sets the value of the distrito property.
	 *
	 * @param value allowed object is
	 * {@link DistritoType}
	 */
	public void setDistrito(DistritoType value)
	{
		this.distrito = value;
	}

	/**
	 * Gets the value of the nome property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getNome()
	{
		return nome;
	}

	/**
	 * Sets the value of the nome property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setNome(String value)
	{
		this.nome = value;
	}

}
