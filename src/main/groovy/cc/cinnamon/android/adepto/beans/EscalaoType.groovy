package cc.cinnamon.android.adepto.beans

import cc.cinnamon.android.adepto.validation.Pattern
import jodd.vtor.constraint.NotNull

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlType

/**
 * <p>Java class for escalaoType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="escalaoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="nome" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="genero"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;enumeration value="F"/&gt;
 *               &lt;enumeration value="M"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="modalidade" type="{}modalidadeType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "escalaoType", propOrder = [
		"id",
		"nome",
		"genero",
		"modalidade"
])
public class EscalaoType
{

	@XmlElement(required = true)
	@NotNull
	protected BigInteger id;

	@XmlElement(required = true)
	@NotNull
	protected String nome;

	@XmlElement(required = true)
	@NotNull
	@Pattern(regexp = ["F", "M"])
	protected String genero;

	@XmlElement(required = true)
	@NotNull
    protected ModalidadeType modalidade;

	/**
	 * Gets the value of the id property.
	 *
	 * @return possible object is
	 * {@link BigInteger}
	 */
	public BigInteger getId()
	{
		return id;
	}

	/**
	 * Sets the value of the id property.
	 *
	 * @param value allowed object is
	 * {@link BigInteger}
	 */
	public void setId(BigInteger value)
	{
		this.id = value;
	}

	/**
	 * Gets the value of the nome property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getNome()
	{
		return nome;
	}

	/**
	 * Sets the value of the nome property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setNome(String value)
	{
		this.nome = value;
	}

	/**
	 * Gets the value of the genero property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getGenero()
	{
		return genero;
	}

	/**
	 * Sets the value of the genero property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setGenero(String value)
	{
		this.genero = value;
	}

	/**
	 * Gets the value of the modalidade property.
	 *
     * @return
     *     possible object is {@link ModalidadeType }
     *
	 */
    public ModalidadeType getModalidade() {
		return modalidade;
	}

	/**
	 * Sets the value of the modalidade property.
	 *
     * @param value
     *     allowed object is {@link ModalidadeType }
     *
	 */
    public void setModalidade(ModalidadeType value) {
		this.modalidade = value;
	}

}
