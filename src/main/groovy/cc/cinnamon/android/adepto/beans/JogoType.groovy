package cc.cinnamon.android.adepto.beans

import jodd.vtor.constraint.NotNull

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlSchemaType
import javax.xml.bind.annotation.XmlType
import javax.xml.datatype.XMLGregorianCalendar

/**
 * <p>Java class for jogoType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="jogoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="fase" type="{}faseType"/&gt;
 *         &lt;element name="data" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="recinto" type="{}recintoType"/&gt;
 *         &lt;element name="hora" type="{http://www.w3.org/2001/XMLSchema}time" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "jogoType", propOrder = [
		"id",
		"fase",
		"data",
		"recinto",
		"hora"
])
public class JogoType
{

	@XmlElement(required = true)
	@NotNull
	protected BigInteger id;

	@XmlElement(required = true)
	@NotNull
	protected FaseType fase;

	@XmlElement(required = true)
	@XmlSchemaType(name = "date")
	@NotNull
	protected XMLGregorianCalendar data;

	@XmlElement(required = true)
	@NotNull
	protected RecintoType recinto;

	@XmlSchemaType(name = "time")
	protected XMLGregorianCalendar hora;

	/**
	 * Gets the value of the id property.
	 *
	 * @return possible object is
	 * {@link BigInteger}
	 */
	public BigInteger getId()
	{
		return id;
	}

	/**
	 * Sets the value of the id property.
	 *
	 * @param value allowed object is
	 * {@link BigInteger}
	 */
	public void setId(BigInteger value)
	{
		this.id = value;
	}

	/**
	 * Gets the value of the fase property.
	 *
	 * @return possible object is
	 * {@link FaseType}
	 */
	public FaseType getFase()
	{
		return fase;
	}

	/**
	 * Sets the value of the fase property.
	 *
	 * @param value allowed object is
	 * {@link FaseType}
	 */
	public void setFase(FaseType value)
	{
		this.fase = value;
	}

	/**
	 * Gets the value of the data property.
	 *
	 * @return possible object is
	 * {@link XMLGregorianCalendar}
	 */
	public XMLGregorianCalendar getData()
	{
		return data;
	}

	/**
	 * Sets the value of the data property.
	 *
	 * @param value allowed object is
	 * {@link XMLGregorianCalendar}
	 */
	public void setData(XMLGregorianCalendar value)
	{
		this.data = value;
	}

	/**
	 * Gets the value of the recinto property.
	 *
	 * @return possible object is
	 * {@link RecintoType}
	 */
	public RecintoType getRecinto()
	{
		return recinto;
	}

	/**
	 * Sets the value of the recinto property.
	 *
	 * @param value allowed object is
	 * {@link RecintoType}
	 */
	public void setRecinto(RecintoType value)
	{
		this.recinto = value;
	}

	/**
	 * Gets the value of the hora property.
	 *
	 * @return possible object is
	 * {@link XMLGregorianCalendar}
	 */
	public XMLGregorianCalendar getHora()
	{
		return hora;
	}

	/**
	 * Sets the value of the hora property.
	 *
	 * @param value allowed object is
	 * {@link XMLGregorianCalendar}
	 */
	public void setHora(XMLGregorianCalendar value)
	{
		this.hora = value;
	}

}
