package cc.cinnamon.android.adepto.beans

import jodd.vtor.constraint.NotNull

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlType

/**
 * <p>Java class for classificacaoType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="classificacaoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="campeonato" type="{}campeonatoType"/&gt;
 *         &lt;element name="equipa" type="{}equipaType"/&gt;
 *         &lt;element name="pontos" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="gsofridos" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="gmarcados" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="empates" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "classificacaoType", propOrder = [
		"id",
		"campeonato",
		"equipa",
		"pontos",
		"gsofridos",
		"gmarcados",
		"empates"
])
public class ClassificacaoType
{

	@XmlElement(required = true)
	@NotNull
	protected BigInteger id;

	@XmlElement(required = true)
	@NotNull
	protected CampeonatoType campeonato;

	@XmlElement(required = true)
	@NotNull
	protected EquipaType equipa;

	protected BigInteger pontos;

	protected BigInteger gsofridos;

	protected BigInteger gmarcados;

	protected BigInteger empates;

	/**
	 * Gets the value of the id property.
	 *
	 * @return possible object is
	 * {@link BigInteger}
	 */
	public BigInteger getId()
	{
		return id;
	}

	/**
	 * Sets the value of the id property.
	 *
	 * @param value allowed object is
	 * {@link BigInteger}
	 */
	public void setId(BigInteger value)
	{
		this.id = value;
	}

	/**
	 * Gets the value of the campeonato property.
	 *
	 * @return possible object is
	 * {@link CampeonatoType}
	 */
	public CampeonatoType getCampeonato()
	{
		return campeonato;
	}

	/**
	 * Sets the value of the campeonato property.
	 *
	 * @param value allowed object is
	 * {@link CampeonatoType}
	 */
	public void setCampeonato(CampeonatoType value)
	{
		this.campeonato = value;
	}

	/**
	 * Gets the value of the equipa property.
	 *
	 * @return possible object is
	 * {@link EquipaType}
	 */
	public EquipaType getEquipa()
	{
		return equipa;
	}

	/**
	 * Sets the value of the equipa property.
	 *
	 * @param value allowed object is
	 * {@link EquipaType}
	 */
	public void setEquipa(EquipaType value)
	{
		this.equipa = value;
	}

	/**
	 * Gets the value of the pontos property.
	 *
	 * @return possible object is
	 * {@link BigInteger}
	 */
	public BigInteger getPontos()
	{
		return pontos;
	}

	/**
	 * Sets the value of the pontos property.
	 *
	 * @param value allowed object is
	 * {@link BigInteger}
	 */
	public void setPontos(BigInteger value)
	{
		this.pontos = value;
	}

	/**
	 * Gets the value of the gsofridos property.
	 *
	 * @return possible object is
	 * {@link BigInteger}
	 */
	public BigInteger getGsofridos()
	{
		return gsofridos;
	}

	/**
	 * Sets the value of the gsofridos property.
	 *
	 * @param value allowed object is
	 * {@link BigInteger}
	 */
	public void setGsofridos(BigInteger value)
	{
		this.gsofridos = value;
	}

	/**
	 * Gets the value of the gmarcados property.
	 *
	 * @return possible object is
	 * {@link BigInteger}
	 */
	public BigInteger getGmarcados()
	{
		return gmarcados;
	}

	/**
	 * Sets the value of the gmarcados property.
	 *
	 * @param value allowed object is
	 * {@link BigInteger}
	 */
	public void setGmarcados(BigInteger value)
	{
		this.gmarcados = value;
	}

	/**
	 * Gets the value of the empates property.
	 *
	 * @return possible object is
	 * {@link BigInteger}
	 */
	public BigInteger getEmpates()
	{
		return empates;
	}

	/**
	 * Sets the value of the empates property.
	 *
	 * @param value allowed object is
	 * {@link BigInteger}
	 */
	public void setEmpates(BigInteger value)
	{
		this.empates = value;
	}

}
