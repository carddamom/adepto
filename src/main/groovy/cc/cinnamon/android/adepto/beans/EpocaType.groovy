package cc.cinnamon.android.adepto.beans

import jodd.vtor.constraint.NotNull

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlSchemaType
import javax.xml.bind.annotation.XmlType
import javax.xml.datatype.XMLGregorianCalendar

/**
 * <p>Java class for epocaType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="epocaType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="campeonato" type="{}campeonatoType"/&gt;
 *         &lt;element name="ano" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="data_inicio" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="data_fim" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "epocaType", propOrder = [
		"id",
		"campeonato",
		"ano",
		"dataInicio",
		"dataFim"
])
public class EpocaType
{

	@XmlElement(required = true)
	@NotNull
	protected BigInteger id;

	@XmlElement(required = true)
	@NotNull
	protected CampeonatoType campeonato;

	@XmlElement(required = true)
	@NotNull
	protected String ano;

	@XmlElement(name = "data_inicio")
	@XmlSchemaType(name = "date")
	protected XMLGregorianCalendar dataInicio;

	@XmlElement(name = "data_fim")
	@XmlSchemaType(name = "date")
	protected XMLGregorianCalendar dataFim;

	/**
	 * Gets the value of the id property.
	 *
	 * @return possible object is
	 * {@link BigInteger}
	 */
	public BigInteger getId()
	{
		return id;
	}

	/**
	 * Sets the value of the id property.
	 *
	 * @param value allowed object is
	 * {@link BigInteger}
	 */
	public void setId(BigInteger value)
	{
		this.id = value;
	}

	/**
	 * Gets the value of the campeonato property.
	 *
	 * @return possible object is
	 * {@link CampeonatoType}
	 */
	public CampeonatoType getCampeonato()
	{
		return campeonato;
	}

	/**
	 * Sets the value of the campeonato property.
	 *
	 * @param value allowed object is
	 * {@link CampeonatoType}
	 */
	public void setCampeonato(CampeonatoType value)
	{
		this.campeonato = value;
	}

	/**
	 * Gets the value of the ano property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getAno()
	{
		return ano;
	}

	/**
	 * Sets the value of the ano property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setAno(String value)
	{
		this.ano = value;
	}

	/**
	 * Gets the value of the dataInicio property.
	 *
	 * @return possible object is
	 * {@link XMLGregorianCalendar}
	 */
	public XMLGregorianCalendar getDataInicio()
	{
		return dataInicio;
	}

	/**
	 * Sets the value of the dataInicio property.
	 *
	 * @param value allowed object is
	 * {@link XMLGregorianCalendar}
	 */
	public void setDataInicio(XMLGregorianCalendar value)
	{
		this.dataInicio = value;
	}

	/**
	 * Gets the value of the dataFim property.
	 *
	 * @return possible object is
	 * {@link XMLGregorianCalendar}
	 */
	public XMLGregorianCalendar getDataFim()
	{
		return dataFim;
	}

	/**
	 * Sets the value of the dataFim property.
	 *
	 * @param value allowed object is
	 * {@link XMLGregorianCalendar}
	 */
	public void setDataFim(XMLGregorianCalendar value)
	{
		this.dataFim = value;
	}

}
