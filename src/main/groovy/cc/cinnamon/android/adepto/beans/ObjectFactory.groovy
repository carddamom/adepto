package cc.cinnamon.android.adepto.beans

import javax.xml.bind.JAXBElement
import javax.xml.bind.annotation.XmlElementDecl
import javax.xml.bind.annotation.XmlRegistry
import javax.xml.namespace.QName
import cc.cinnamon.android.adepto.beans.bulk.*

/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the cc.cinnamon.android.adepto.beans package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
@SuppressWarnings("GroovyUnusedDeclaration")
public class ObjectFactory
{

	private final static QName _Clube_QNAME = new QName("", "clube");
	private final static QName _Freguesia_QNAME = new QName("", "freguesia");
	private final static QName _Concelho_QNAME = new QName("", "concelho");
	private final static QName _Distrito_QNAME = new QName("", "distrito");
	private final static QName _Pais_QNAME = new QName("", "pais");
	private final static QName _Equipa_QNAME = new QName("", "equipa");
	private final static QName _Escalao_QNAME = new QName("", "escalao");
	private final static QName _Modalidade_QNAME = new QName("", "modalidade");
	private final static QName _Classificacao_QNAME = new QName("", "classificacao");
	private final static QName _Campeonato_QNAME = new QName("", "campeonato");
	private final static QName _Epoca_QNAME = new QName("", "epoca");
	private final static QName _Fase_QNAME = new QName("", "fase");
	private final static QName _Jogo_QNAME = new QName("", "jogo");
	private final static QName _Resultado_QNAME = new QName("", "resultado");
	private final static QName _ResultadoJogador_QNAME = new QName("", "resultado_jogador");
	private final static QName _Jogador_QNAME = new QName("", "jogador");
	private final static QName _CampeonatoJogador_QNAME = new QName("", "campeonato_jogador");
	private final static QName _JogadorEquipa_QNAME = new QName("", "jogador_equipa");
	private final static QName _Recinto_QNAME = new QName("", "recinto");
	private final static QName _Noticia_QNAME = new QName("", "noticia");

	/**
	 * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cc.cinnamon.android.adepto.beans
	 */
	private ObjectFactory()
	{
	}

	/**
	 * Create an instance of {@link ClubeType}
	 */
	public static ClubeType createClubeType()
	{
		return new ClubeType();
	}

	/**
	 * Create an instance of {@link FreguesiaType}
	 */
	public static FreguesiaType createFreguesiaType()
	{
		return new FreguesiaType();
	}

	/**
	 * Create an instance of {@link ConcelhoType}
	 */
	public static ConcelhoType createConcelhoType()
	{
		return new ConcelhoType();
	}

	/**
	 * Create an instance of {@link DistritoType}
	 */
	public static DistritoType createDistritoType()
	{
		return new DistritoType();
	}

	/**
	 * Create an instance of {@link PaisType}
	 */
	public static PaisType createPaisType()
	{
		return new PaisType();
	}

	/**
	 * Create an instance of {@link EquipaType}
	 */
	public static EquipaType createEquipaType()
	{
		return new EquipaType();
	}

	/**
	 * Create an instance of {@link EscalaoType}
	 */
	public static EscalaoType createEscalaoType()
	{
		return new EscalaoType();
	}

	/**
	 * Create an instance of {@link ModalidadeType}
	 */
	public static ModalidadeType createModalidadeType()
	{
		return new ModalidadeType();
	}

	/**
	 * Create an instance of {@link ClassificacaoType}
	 */
	public static ClassificacaoType createClassificacaoType()
	{
		return new ClassificacaoType();
	}

	/**
	 * Create an instance of {@link CampeonatoType}
	 */
	public static CampeonatoType createCampeonatoType()
	{
		return new CampeonatoType();
	}

	/**
	 * Create an instance of {@link EpocaType}
	 */
	public static EpocaType createEpocaType()
	{
		return new EpocaType();
	}

	/**
	 * Create an instance of {@link FaseType}
	 */
	public static FaseType createFaseType()
	{
		return new FaseType();
	}

	/**
	 * Create an instance of {@link JogoType}
	 */
	public static JogoType createJogoType()
	{
		return new JogoType();
	}

	/**
	 * Create an instance of {@link ResultadoType}
	 */
	public static ResultadoType createResultadoType()
	{
		return new ResultadoType();
	}

	/**
	 * Create an instance of {@link ResultadoJogadorType}
	 */
	public static ResultadoJogadorType createResultadoJogadorType()
	{
		return new ResultadoJogadorType();
	}

	/**
	 * Create an instance of {@link JogadorType}
	 */
	public static JogadorType createJogadorType()
	{
		return new JogadorType();
	}

	/**
	 * Create an instance of {@link CampeonatoJogadorType}
	 */
	public static CampeonatoJogadorType createCampeonatoJogadorType()
	{
		return new CampeonatoJogadorType();
	}

	/**
	 * Create an instance of {@link JogadorEquipaType}
	 */
	public static JogadorEquipaType createJogadorEquipaType()
	{
		return new JogadorEquipaType();
	}

	/**
	 * Create an instance of {@link RecintoType}
	 */
	public static RecintoType createRecintoType()
	{
		return new RecintoType();
	}

	/**
	 * Create an instance of {@link NoticiasType}
	 */
	public static NoticiasType createNoticiasType()
	{
		return new NoticiasType();
	}

	/**
	 * Create an instance of {@link Clubes}
	 */
	public static Clubes createClubes()
	{
		return new Clubes();
	}

	/**
	 * Create an instance of {@link Freguesias}
	 */
	public static Freguesias createFreguesias()
	{
		return new Freguesias();
	}

	/**
	 * Create an instance of {@link Concelhos}
	 */
	public static Concelhos createConcelhos()
	{
		return new Concelhos();
	}

	/**
	 * Create an instance of {@link Distritos}
	 */
	public static Distritos createDistritos()
	{
		return new Distritos();
	}

	/**
	 * Create an instance of {@link Paises}
	 */
	public static Paises createPaises()
	{
		return new Paises();
	}

	/**
	 * Create an instance of {@link Equipas}
	 */
	public static Equipas createEquipas()
	{
		return new Equipas();
	}

	/**
	 * Create an instance of {@link Escalaoes}
	 */
	public static Escalaoes createEscalaoes()
	{
		return new Escalaoes();
	}

	/**
	 * Create an instance of {@link Modalidades}
	 */
	public static Modalidades createModalidades()
	{
		return new Modalidades();
	}

	/**
	 * Create an instance of {@link Classificacoes}
	 */
	public static Classificacoes createClassificacoes()
	{
		return new Classificacoes();
	}

	/**
	 * Create an instance of {@link Campeonatos}
	 */
	public static Campeonatos createCampeonatos()
	{
		return new Campeonatos();
	}

	/**
	 * Create an instance of {@link Epocas}
	 */
	public static Epocas createEpocas()
	{
		return new Epocas();
	}

	/**
	 * Create an instance of {@link Fases}
	 */
	public static Fases createFases()
	{
		return new Fases();
	}

	/**
	 * Create an instance of {@link Jogos}
	 */
	public static Jogos createJogos()
	{
		return new Jogos();
	}

	/**
	 * Create an instance of {@link Resultados}
	 */
	public static Resultados createResultados()
	{
		return new Resultados();
	}

	/**
	 * Create an instance of {@link ResultadosJogadores}
	 */
	public static ResultadosJogadores createResultadosJogadores()
	{
		return new ResultadosJogadores();
	}

	/**
	 * Create an instance of {@link Jogadores}
	 */
	public static Jogadores createJogadores()
	{
		return new Jogadores();
	}

	/**
	 * Create an instance of {@link CampeonatosJogadores}
	 */
	public static CampeonatosJogadores createCampeonatosJogadores()
	{
		return new CampeonatosJogadores();
	}

	/**
	 * Create an instance of {@link JogadoresEquipas}
	 */
	public static JogadoresEquipas createJogadoresEquipas()
	{
		return new JogadoresEquipas();
	}

	/**
	 * Create an instance of {@link Recintos}
	 */
	public static Recintos createRecintos()
	{
		return new Recintos();
	}

	/**
	 * Create an instance of {@link Noticias}
	 */
	public static Noticias createNoticias()
	{
		return new Noticias();
	}

	/**
	 * Create an instance of {@link EquipamentoType}
	 */
	public static EquipamentoType createEquipamentoType()
	{
		return new EquipamentoType();
	}

	/**
	 * Create an instance of {@link JAXBElement} {@code <} {@link ClubeType} {@code >}}
	 */
	@XmlElementDecl(namespace = "", name = "clube")
	public static JAXBElement<ClubeType> createClube(ClubeType value)
	{
		return new JAXBElement<ClubeType>(_Clube_QNAME, ClubeType.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement} {@code <} {@link FreguesiaType} {@code >}}
	 */
	@XmlElementDecl(namespace = "", name = "freguesia")
	public static JAXBElement<FreguesiaType> createFreguesia(FreguesiaType value)
	{
		return new JAXBElement<FreguesiaType>(_Freguesia_QNAME, FreguesiaType.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement} {@code <} {@link ConcelhoType} {@code >}}
	 */
	@XmlElementDecl(namespace = "", name = "concelho")
	public static JAXBElement<ConcelhoType> createConcelho(ConcelhoType value)
	{
		return new JAXBElement<ConcelhoType>(_Concelho_QNAME, ConcelhoType.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement} {@code <} {@link DistritoType} {@code >}}
	 */
	@XmlElementDecl(namespace = "", name = "distrito")
	public static JAXBElement<DistritoType> createDistrito(DistritoType value)
	{
		return new JAXBElement<DistritoType>(_Distrito_QNAME, DistritoType.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement} {@code <} {@link PaisType} {@code >}}
	 */
	@XmlElementDecl(namespace = "", name = "pais")
	public static JAXBElement<PaisType> createPais(PaisType value)
	{
		return new JAXBElement<PaisType>(_Pais_QNAME, PaisType.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement} {@code <} {@link EquipaType} {@code >}}
	 */
	@XmlElementDecl(namespace = "", name = "equipa")
	public static JAXBElement<EquipaType> createEquipa(EquipaType value)
	{
		return new JAXBElement<EquipaType>(_Equipa_QNAME, EquipaType.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement} {@code <} {@link EscalaoType} {@code >}}
	 */
	@XmlElementDecl(namespace = "", name = "escalao")
	public static JAXBElement<EscalaoType> createEscalao(EscalaoType value)
	{
		return new JAXBElement<EscalaoType>(_Escalao_QNAME, EscalaoType.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement} {@code <} {@link ModalidadeType} {@code >}}
	 */
	@XmlElementDecl(namespace = "", name = "modalidade")
	public static JAXBElement<ModalidadeType> createModalidade(ModalidadeType value)
	{
		return new JAXBElement<ModalidadeType>(_Modalidade_QNAME, ModalidadeType.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement} {@code <} {@link ClassificacaoType} {@code >}}
	 */
	@XmlElementDecl(namespace = "", name = "classificacao")
	public static JAXBElement<ClassificacaoType> createClassificacao(ClassificacaoType value)
	{
		return new JAXBElement<ClassificacaoType>(_Classificacao_QNAME, ClassificacaoType.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement} {@code <} {@link CampeonatoType} {@code >}}
	 */
	@XmlElementDecl(namespace = "", name = "campeonato")
	public static JAXBElement<CampeonatoType> createCampeonato(CampeonatoType value)
	{
		return new JAXBElement<CampeonatoType>(_Campeonato_QNAME, CampeonatoType.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement} {@code <} {@link EpocaType} {@code >}}
	 */
	@XmlElementDecl(namespace = "", name = "epoca")
	public static JAXBElement<EpocaType> createEpoca(EpocaType value)
	{
		return new JAXBElement<EpocaType>(_Epoca_QNAME, EpocaType.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement} {@code <} {@link FaseType} {@code >}}
	 */
	@XmlElementDecl(namespace = "", name = "fase")
	public static JAXBElement<FaseType> createFase(FaseType value)
	{
		return new JAXBElement<FaseType>(_Fase_QNAME, FaseType.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement} {@code <} {@link JogoType} {@code >}}
	 */
	@XmlElementDecl(namespace = "", name = "jogo")
	public static JAXBElement<JogoType> createJogo(JogoType value)
	{
		return new JAXBElement<JogoType>(_Jogo_QNAME, JogoType.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement} {@code <} {@link ResultadoType} {@code >}}
	 */
	@XmlElementDecl(namespace = "", name = "resultado")
	public static JAXBElement<ResultadoType> createResultado(ResultadoType value)
	{
		return new JAXBElement<ResultadoType>(_Resultado_QNAME, ResultadoType.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement} {@code <} {@link ResultadoJogadorType} {@code >}}
	 */
	@XmlElementDecl(namespace = "", name = "resultado_jogador")
	public static JAXBElement<ResultadoJogadorType> createResultadoJogador(ResultadoJogadorType value)
	{
		return new JAXBElement<ResultadoJogadorType>(_ResultadoJogador_QNAME, ResultadoJogadorType.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement} {@code <} {@link JogadorType} {@code >}}
	 */
	@XmlElementDecl(namespace = "", name = "jogador")
	public static JAXBElement<JogadorType> createJogador(JogadorType value)
	{
		return new JAXBElement<JogadorType>(_Jogador_QNAME, JogadorType.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement} {@code <} {@link CampeonatoJogadorType} {@code >}}
	 */
	@XmlElementDecl(namespace = "", name = "campeonato_jogador")
	public static JAXBElement<CampeonatoJogadorType> createCampeonatoJogador(CampeonatoJogadorType value)
	{
		return new JAXBElement<CampeonatoJogadorType>(_CampeonatoJogador_QNAME, CampeonatoJogadorType.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement} {@code <} {@link JogadorEquipaType} {@code >}}
	 */
	@XmlElementDecl(namespace = "", name = "jogador_equipa")
	public static JAXBElement<JogadorEquipaType> createJogadorEquipa(JogadorEquipaType value)
	{
		return new JAXBElement<JogadorEquipaType>(_JogadorEquipa_QNAME, JogadorEquipaType.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement} {@code <} {@link RecintoType} {@code >}}
	 */
	@XmlElementDecl(namespace = "", name = "recinto")
	public static JAXBElement<RecintoType> createRecinto(RecintoType value)
	{
		return new JAXBElement<RecintoType>(_Recinto_QNAME, RecintoType.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement} {@code <} {@link NoticiasType} {@code >}}
	 */
	@XmlElementDecl(namespace = "", name = "noticia")
	public static JAXBElement<NoticiasType> createNoticia(NoticiasType value)
	{
		return new JAXBElement<NoticiasType>(_Noticia_QNAME, NoticiasType.class, null, value);
	}

}
