package cc.cinnamon.android.adepto.beans

import jodd.vtor.constraint.NotNull

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlSchemaType
import javax.xml.bind.annotation.XmlType
import javax.xml.datatype.XMLGregorianCalendar

/**
 * <p>Java class for jogadorType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="jogadorType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="nome" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="data_nascimento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="foto" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="nacionalidade" type="{}paisType" minOccurs="0"/&gt;
 *         &lt;element name="naturalidade" type="{}paisType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "jogadorType", propOrder = [
		"id",
		"nome",
		"dataNascimento",
		"foto",
		"nacionalidade",
		"naturalidade"
])
public class JogadorType
{

	@XmlElement(required = true)
	@NotNull
	protected BigInteger id;

	@XmlElement(required = true)
	@NotNull
	protected String nome;

 @XmlElement(name = "data_nascimento")
	@XmlSchemaType(name = "date")
	protected XMLGregorianCalendar dataNascimento;

	protected byte[] foto;

	protected PaisType nacionalidade;

	protected PaisType naturalidade;

	/**
	 * Gets the value of the id property.
	 *
	 * @return possible object is
	 * {@link BigInteger}
	 */
	public BigInteger getId()
	{
		return id;
	}

	/**
	 * Sets the value of the id property.
	 *
	 * @param value allowed object is
	 * {@link BigInteger}
	 */
	public void setId(BigInteger value)
	{
		this.id = value;
	}

	/**
	 * Gets the value of the nome property.
	 *
	 * @return possible object is
	 * {@link String}
	 */
	public String getNome()
	{
		return nome;
	}

	/**
	 * Sets the value of the nome property.
	 *
	 * @param value allowed object is
	 * {@link String}
	 */
	public void setNome(String value)
	{
		this.nome = value;
	}

	/**
	 * Gets the value of the dataNascimento property.
	 *
	 * @return possible object is
	 * {@link XMLGregorianCalendar}
	 */
	public XMLGregorianCalendar getDataNascimento()
	{
		return dataNascimento;
	}

	/**
	 * Sets the value of the dataNascimento property.
	 *
	 * @param value allowed object is
	 * {@link XMLGregorianCalendar}
	 */
	public void setDataNascimento(XMLGregorianCalendar value)
	{
		this.dataNascimento = value;
	}

	/**
	 * Gets the value of the foto property.
	 *
	 * @return possible object is
	 * byte[]
	 */
	public byte[] getFoto()
	{
		return foto;
	}

	/**
	 * Sets the value of the foto property.
	 *
	 * @param value allowed object is
	 *              byte[]
	 */
	public void setFoto(byte[] value)
	{
		this.foto = value;
	}

	/**
	 * Gets the value of the nacionalidade property.
	 *
	 * @return possible object is
	 * {@link PaisType}
	 */
	public PaisType getNacionalidade()
	{
		return nacionalidade;
	}

	/**
	 * Sets the value of the nacionalidade property.
	 *
	 * @param value allowed object is
	 * {@link PaisType}
	 */
	public void setNacionalidade(PaisType value)
	{
		this.nacionalidade = value;
	}

	/**
	 * Gets the value of the naturalidade property.
	 *
	 * @return possible object is
	 * {@link PaisType}
	 */
	public PaisType getNaturalidade()
	{
		return naturalidade;
	}

	/**
	 * Sets the value of the naturalidade property.
	 *
	 * @param value allowed object is
	 * {@link PaisType}
	 */
	public void setNaturalidade(PaisType value)
	{
		this.naturalidade = value;
	}

}
