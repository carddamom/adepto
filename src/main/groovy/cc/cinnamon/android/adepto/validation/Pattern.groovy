package cc.cinnamon.android.adepto.validation

public @interface Pattern
{

	String[] regexp();

}
