# Domain Notes for Adepto

+ clube => id (serial), nome (varchar), abreviatura (varchar), morada (varchar),
  localidade (varchar), contactos (varchar), email (varchar), data_fundacao (date),
  telefone (varchar), fax (varchar), website (url), freguesia (id),
  cor (varchar (android colour)), logo (base64 bytea), codigo_postal (varchar);

+ freguesia => id (serial), concelho (integer), nome (varchar);

+ concelho => id (serial), distrito (integer), nome (varchar);

+ distrito => id (serial), pais (integer), nome (varchar);

+ pais => id (serial), nome (varchar), nacionalidade (varchar), naturalidade (varchar);

+ equipa => id (serial), clube (integer), nome (varchar), escalao (integer),
  genero (varchar), equipamento (base64 bytea);

+ escalao => id (serial), nome (varchar), genero (varchar), modalidade (integer);

+ modalidade => id (serial), nome (varchar), icon (base64 bytea);

+ classificacao => id (serial), campeonato (integer), equipa (integer), pontos (integer),
  gsofridos (integer), gmarcados (integer), empates (integer);

+ campeonato => id (serial), nome (varchar), logo (base64 bytea), patrocinador (varchar),
  escalao (integer), descricao (varchar);

+ epoca => id (serial), campeonato (integer), ano (varchar), data_inicio (data),
  data_fim (data);

+ fase => id (serial), epoca (integer), nome (varchar), descricao (varchar);

+ jogo => id (serial), fase (integer), data (date), recinto (integer), hora (hour);

+ resultado => id (serial), jogo (integer), equipa (integer), resultado (integer),
  final (boolean), descricao (varchar);

+ resultado_jogador => id (serial), resultado (integer), jogador (integer),
  minutos (integer), autogolo (boolean);

+ jogador => id (serial), nome (varchar), data_nascimento (date),
  foto (base64 bytea), nacionalidade (integer), naturalidade (integer);

+ campeonato_jogador => id (serial), jogador (integer), campeonato (integer),
  pontuacao (integer);

+ jogador_equipa => id (serial), jogador (integer), posicao (string), numero (integer),
  capitao (boolean), data (date);

+ noticias => id (serial), clube (integer), fonte (string), titulo (string),
  resumo (string), link (URL);

+ recinto => id (serial), nome (varchar), morada (varchar), freguesia (integer),
codigo_postal (varchar), localidade (varchar), website (url), foto (base64 bytea);
